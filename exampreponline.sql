-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2017 at 02:54 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exampreponline`
--

-- --------------------------------------------------------

--
-- Table structure for table `task1`
--

CREATE TABLE `task1` (
  `id` int(11) NOT NULL,
  `exam` varchar(1024) NOT NULL,
  `location` varchar(64) NOT NULL,
  `offline` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `task2`
--

CREATE TABLE `task2` (
  `id` int(11) NOT NULL,
  `location` varchar(128) NOT NULL,
  `organisation` varchar(64) NOT NULL,
  `stream` varchar(128) NOT NULL,
  `a_mode` varchar(32) NOT NULL,
  `s_procedure` varchar(1024) NOT NULL,
  `t_interview` varchar(1024) NOT NULL,
  `a_question` varchar(1024) NOT NULL,
  `h_question` varchar(1024) NOT NULL,
  `suggestion` varchar(1024) NOT NULL,
  `shared_by` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task2`
--

INSERT INTO `task2` (`id`, `location`, `organisation`, `stream`, `a_mode`, `s_procedure`, `t_interview`, `a_question`, `h_question`, `suggestion`, `shared_by`) VALUES
(1, 'Chennai', 'ARW', 'Computer Science Engineering', 'On-campus', 'Looking for candidates strong in web development. They conduct aptitude test and have designing round also', 'They ask about recent project and technologies you have worked with.', 'They usually ask question on profit or loss.', 'Questions on strength and weakness.', 'Have clear understanding of web development.', 'Radha Nair'),
(2, 'Kolkata', 'ARW', 'Computer Science Engineering', 'Off-campus', 'Looking for candidates strong in web development. They conduct aptitude test and have designing round also', 'They ask about recent project and technologies you have worked with.', 'They usually ask question on profit or loss.', 'Questions on strength and weakness.', 'Have clear understanding of web development.', 'Kiran Sharma'),
(3, 'Chennai', 'Google', 'Computer Science Engineering', 'Off-campus', 'Looking for candidates strong in web development. They conduct aptitude test and have designing round also', 'They ask about recent project and technologies you have worked with.', 'They usually ask question on profit or loss.', 'Questions on strength and weakness.', 'Have clear understanding of web development.', 'Kiran Sharma'),
(4, 'Delhi', 'BHEL', 'Mechanical Engineering', 'On-campus', 'Looking for candidates strong in automobile designing. ', 'They ask about recent worshops and technologies you have worked with.', 'They usually ask question on materials and thermodynamics.', 'Questions on strength and weakness.', 'Have clear understanding of automobile designing.', 'Richa Mishra');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `task1`
--
ALTER TABLE `task1`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `exam_unique` (`exam`);

--
-- Indexes for table `task2`
--
ALTER TABLE `task2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `task1`
--
ALTER TABLE `task1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `task2`
--
ALTER TABLE `task2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
