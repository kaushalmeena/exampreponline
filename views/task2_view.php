<div class="thumbnail">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-3">
                <div class="thumbnail padding-lg">
                    <form method="get" action="task2.php">
                        <div class="text-right">
                            <input type="submit" class="btn btn-link btn-sm" value="Apply filter">
                        </div>
                        <div class="form-group">
                            <label for="formSelect1">Organisation:</label>
                            <select name="organisation" id="formSelect1" class="form-control input-sm">
                                <option value="" selected>-All-</option>
                                <?php if (count($organisations) > 0): ?>
                                <?php foreach($organisations as $organisation): ?>
                                <option value="<?php echo $organisation['organisation']; ?>" <?php if (isset($selectedOrganisation)) if($selectedOrganisation == $organisation['organisation']) echo 'selected'; ?>><?php echo $organisation['organisation']; ?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="formSelect2">Enginering Stream:</label>
                            <select name="stream" id="formSelect2" class="form-control input-sm">
                                <option value="" selected>-All-</option>
                                <?php if (count($streams) > 0): ?>
                                <?php foreach($streams as $stream): ?>
                                <option value="<?php echo $stream['stream']; ?>" <?php if (isset($selectedStream)) if($selectedStream == $stream['stream']) echo 'selected'; ?>><?php echo $stream['stream']; ?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="formSelect3">Application Mode:</label>
                            <select name="a_mode" id="formSelect3" class="form-control input-sm">
                                <option value="" selected>-All-</option>
                                <?php if (count($a_modes) > 0): ?>
                                <?php foreach($a_modes as $a_mode): ?>
                                <option value="<?php echo $a_mode['a_mode']; ?>" <?php if (isset($selectedMode)) if($selectedMode == $a_mode['a_mode']) echo 'selected'; ?>><?php echo $a_mode['a_mode']; ?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="thumbnail padding-lg">
                    <div class="text-right">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-link btn-sm" onclick="uncheckAll()">Clear all</button>
                            <button type="button" class="btn btn-link btn-sm" onclick="checkAll()">Select all</button>
                        </div>
                    </div>
                    <div class="form-check">
                        <label for="formCheckbox1" class="form-check-label">
                            <input id="formCheckbox1" type="checkbox" class="form-check-input" onchange="checkboxAction('.s_procedure')" checked> Selection Procedure
                        </label>
                    </div>
                    <div class="form-check">
                        <label for="formCheckbox2" class="form-check-label">
                            <input id="formCheckbox2" type="checkbox" class="form-check-input" onchange="checkboxAction('.t_interview')" checked> Technical Interview
                        </label>
                    </div>
                    <div class="form-check">
                        <label for="formCheckbox3" class="form-check-label">
                            <input id="formCheckbox3" type="checkbox" class="form-check-input" onchange="checkboxAction('.a_question')" checked> Analytical Questions
                        </label>
                    </div>
                    <div class="form-check">
                        <label for="formCheckbox4" class="form-check-label">
                            <input id="formCheckbox4" type="checkbox" class="form-check-input" onchange="checkboxAction('.h_question')" checked> HR Questions
                        </label>
                    </div>
                    <div class="form-check">
                        <label for="formCheckbox5" class="form-check-label">
                            <input id="formCheckbox5" type="checkbox" class="form-check-input" onchange="checkboxAction('.suggestion')" checked> Sugggestions
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <h2 class="text-center">INTERVIEW EXPERIENCES<?php if (isset($selectedOrganisation)): ?><span> :: <?php echo $selectedOrganisation; ?></span><?php endif; ?></h2>
                <?php if (count($jobs) > 0): ?>
                <?php foreach($jobs as $job): ?>
                <div class="well">
                    <strong>Job Location:</strong>&nbsp;<?php echo $job['location']; ?>
                    <br>
                    <strong>Organisation:</strong>&nbsp;<?php echo $job['organisation']; ?>
                    <br>
                    <strong>Application Mode:</strong>&nbsp;<?php echo $job['a_mode']; ?>
                    <br>
                    <span class="s_procedure">
                    <strong>Selection Procedure:</strong>&nbsp;<?php echo $job['s_procedure']; ?>
                    <br>
                    </span>
                    <span class="t_interview">
                    <strong>Technical Interview:</strong>&nbsp;<?php echo $job['t_interview']; ?>
                    <br>
                    </span>
                    <span class="a_question">
                    <strong>Analytical Questions:</strong>&nbsp;<?php echo $job['a_question']; ?>
                    <br>
                    </span>
                    <span class="h_question">
                    <strong>HR Questions:</strong>&nbsp;<?php echo $job['h_question']; ?>
                    <br>
                    </span>
                    <span class="suggestion">
                    <strong>Suggestions:</strong>&nbsp;<?php echo $job['suggestion']; ?>
                    <br>
                    </span>
                    <strong>Shared By:</strong>&nbsp;<?php echo $job['shared_by']; ?>
                    <br>
                </div>
                <?php endforeach; ?>
                <?php else: ?>
                <div class="thumbnail text-center"> [ Empty ]</div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>