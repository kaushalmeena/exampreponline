<div class="thumbnail">
    <div class="jumbotron">
        <form method="post" action="task1.php">
            <div class="form-group">
                <label for="formTextarea">Exam</label>
                <textarea name="exam" id="formTextarea" class="form-control" rows="3" required></textarea>
            </div>
            <div class="form-group">
                <label for="formSelect">Location</label>
                <select name="location" id="formSelect" class="form-control" required>
                    <option value="" selected disabled hidden>-Select-</option>
                    <option value="delhi">Delhi</option>
                    <option value="mumbai">Mumbai</option>
                    <option value="hyderabad">Hyderabad</option>
                    <option value="kolkata">Kolkata</option>
                    <option value="chennai">Chennai</option>
                </select>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input name="offline" type="checkbox" class="form-check-input"> Offline
                </label>
            </div>
            <div class="text-right">
                <input type="submit" class="btn btn-primary btn-min-width">
                <button type="reset" class="btn btn-default btn-min-width">Reset</button>
            </div>
        </form>
    </div>
</div>