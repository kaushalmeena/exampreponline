<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- CUSTOM-FAVICON -->
    <link rel="shortcut icon" href="icon/favicon.ico">
    <!-- CUSTOM-STYLESHEET -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>exampreponline: <?php echo $pageTitle; ?></title>
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-book"></span> ExamPrepOnline</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li <?php if ($pageTitle=='task1' ) echo 'class="active"'?>>
                        <a href="task1.php">task-1</a>
                    </li>
                    <li <?php if ($pageTitle=='task2' ) echo 'class="active"'?>>
                        <a href="task2.php">task-2</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <?php if (isset($_SESSION["message"])): ?>
        <div class="well well-sm">
            <ul>
                <?php foreach($_SESSION["message"] as $message): ?>
                <li>
                    <?php echo $message; ?>
                </li>
                <?php endforeach; ?>
                <?php unset($_SESSION["message"]); ?>
            </ul>
        </div>
        <?php endif; ?>
        <?php require_once("../views/{$view}"); ?>
    </div>

    <footer class="footer">
        <div class="container text-center">
            <span class="text-muted">All rights reserved.</span>
        </div>
    </footer>

    <!-- SCRIPTS -->
    <script src="js/jquery-3.1.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    
</body>

</html>