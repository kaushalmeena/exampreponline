<?php

    require("../includes/config.php"); 
    
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
		$_EXAM = input($_POST["exam"]);
		$_LOCATION = input($_POST["location"]);
        
        if (isset($_POST["offline"])) {
			$_OFFLINE = 1;
		} else {
			$_OFFLINE = 0;
		}
		
		if (empty($_EXAM))
        {
            show_error_page("task1", "Empty exam", "Exam can't be left empty.");
        }
		if (empty($_LOCATION))
        {
            show_error_page("task1", "Empty location", "Location can't be left empty.");
        }
		
        $count_data = query("SELECT * FROM task1 WHERE exam = ?", ['s', $_EXAM], 'n');
        
        if ($count_data >= 1) {
            show_error_page("task1", "Invalid exam", "Specified exam already exists.");
        }
        
		$insert_data = query("INSERT INTO task1 (exam, location, offline) VALUES (?, ?, ?)", ["ssi", $_EXAM, $_LOCATION, $_OFFLINE], 'i');
			
        if ($insert_data) { 
            $_SESSION["message"] = ['Form successfully submitted.']; 
            redirect("task1.php");
        } else {
            show_error_page("task1", "Database error", "Error occured while inserting data.");
        }
	}
	else {
		render("task1_view.php", ['pageTitle' => 'task1']);
	}
?>