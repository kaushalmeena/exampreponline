function checkboxAction(field) {
    $(field).fadeToggle();
}

function checkAll() {
    $("[type=checkbox]").prop("checked", true);
    $(".s_procedure, .t_interview, .a_question, .h_question, .suggestion").fadeIn();
}

function uncheckAll() {
    $("[type=checkbox]").prop("checked", false);
    $(".s_procedure, .t_interview, .a_question, .h_question, .suggestion").fadeOut();
}