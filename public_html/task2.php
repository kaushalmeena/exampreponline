<?php
    require("../includes/config.php"); 
    
    $organisations = query("SELECT DISTINCT organisation FROM task2");
    $streams = query("SELECT DISTINCT stream FROM task2");
    $a_modes = query("SELECT DISTINCT a_mode FROM task2");

    $params = null;
    $first = true;
    $query = 'SELECT * FROM task2';

    if (!empty($_GET['organisation']) || !empty($_GET['stream']) || !empty($_GET['a_mode'])) {
        $params = array();
        $params[] = '';
    }

    if (!empty($_GET['organisation'])) {
        $context['selectedOrganisation'] = $_GET['organisation'];
        
        if ($first) {
            $query = $query.' WHERE organisation = ?';
            $first = false;
        } else {
            $query = $query.' AND organisation = ?';
        }

        $params[0] = $params[0].'s';
        $params[] = $_GET['organisation'];
    }

    if (!empty($_GET['stream'])) {
        $context['selectedStream'] = $_GET['stream'];
        
        if ($first) {
            $query = $query.' WHERE stream = ?';
            $first = false;
        } else {
            $query = $query.' AND stream = ?';
        }

        $params[0] = $params[0].'s';
        $params[] = $_GET['stream'];
    }

    if (!empty($_GET['a_mode'])) {
        $context['selectedMode'] = $_GET['a_mode'];
        
        if ($first) {
            $query = $query.' WHERE a_mode = ?';
            $first = false;
        } else {
            $query = $query.' AND a_mode = ?';
        }

        $params[0] = $params[0].'s';
        $params[] = $_GET['a_mode'];
    }

    $jobs = query($query, $params);

    $context['jobs'] = $jobs;

    $context['organisations'] = $organisations;
    $context['streams'] = $streams;
    $context['a_modes'] = $a_modes;
	$context['pageTitle'] = 'task2';

	render("task2_view.php", $context);
?>